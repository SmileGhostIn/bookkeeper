import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import {Container} from 'react-bootstrap'

import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import LoginScreen from "./screens/LoginScreen";
import RegisterScreen from "./screens/RegisterScreen";
import ProfileScreen from "./screens/ProfileScreen";
import UserListScreen from "./screens/UserListScreen";
import UserEditScreen from "./screens/UserEditScreen";
import ProductListScreen from "./screens/ProductListScreen";
import ProductEditScreen from "./screens/ProductEditScreen";
import PdfViewerScreen from "./screens/PdfViewerScreen";


const App = () => {
  return (
    <BrowserRouter>
      <>
        <Header/>
        <main className='py-3'>
          <Container>
            <Route path='/login' component={LoginScreen}/>
            <Route path='/register' component={RegisterScreen}/>
            <Route path='/profile' component={ProfileScreen}/>
            <Route path='/product/:id' component={ProductScreen} exact/>
            <Route path='/product/:id/viewer' component={PdfViewerScreen} exact/>
            <Route path='/admin/userList' component={UserListScreen}/>
            <Route path='/admin/user/:id/edit' component={UserEditScreen}/>
            <Route path='/admin/productlist' component={ProductListScreen} exact/>
            <Route path='/admin/productlist/:pageNumber' component={ProductListScreen} exact/>
            <Route path='/admin/product/:id/edit' component={ProductEditScreen}/>
            <Route path='/search/:keyword' component={HomeScreen} exact/>
            <Route path='/page/:pageNumber' component={HomeScreen} exact/>
            <Route path='/search/:keyword/page/:pageNumber' component={HomeScreen} exact/>
            <Route path='/' component={HomeScreen} exact/>
          </Container>
        </main>
        <Footer/>
      </>
    </BrowserRouter>
  );
}

export default App;
