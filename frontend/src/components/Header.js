import React from 'react';
import {Route} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {LinkContainer} from 'react-router-bootstrap'
import {Navbar, Nav, Container, NavDropdown} from 'react-bootstrap'
import SearchBox from "./SearchBox";
import {logout} from "../actions/userActions";

const Header = () => {
  const dispatch = useDispatch()

  const userLogin = useSelector(state=> state.userLogin)
  const {userInfo } = userLogin

  const logoutHandler = () => {
    dispatch(logout())
  }

  return (
    <header>
      <Navbar bg="light" expand="lg" collapseOnSelect>
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand>BookKeeper</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Route render={({history}) => <SearchBox history={history} />} />
            <Nav className="ml-auto">
              {userInfo ? (
                <NavDropdown id='username' title={userInfo.name}>
                  <LinkContainer to='/profile'>
                    <NavDropdown.Item>Профиль</NavDropdown.Item>
                  </LinkContainer>
                  <NavDropdown.Item onClick={logoutHandler}>Выход</NavDropdown.Item>
                </NavDropdown>
              ) : <LinkContainer to='/login'>
                <Nav.Link><i className='fas fa-user'></i> Вход</Nav.Link>
              </LinkContainer>}
              {userInfo && userInfo.isAdmin && (
                <NavDropdown id='admin' title='Администратор'>
                  <LinkContainer to='/admin/userlist'>
                    <NavDropdown.Item>Пользователи</NavDropdown.Item>
                  </LinkContainer>
                  <LinkContainer to='/admin/productlist'>
                    <NavDropdown.Item>Продукты</NavDropdown.Item>
                  </LinkContainer>
                </NavDropdown>
              )}
            
            </Nav>
          </Navbar.Collapse>

        </Container>
      </Navbar>
    </header>
  )
}

export default Header;