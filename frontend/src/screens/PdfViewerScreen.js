import React, {useEffect, useState} from "react";
import {Viewer} from '@react-pdf-viewer/core';
import {defaultLayoutPlugin} from "@react-pdf-viewer/default-layout";
import {Worker} from "@react-pdf-viewer/core";
import {useDispatch, useSelector} from "react-redux";

import '@react-pdf-viewer/default-layout/lib/styles/index.css'
import '@react-pdf-viewer/core/lib/styles/index.css'
import {listProductDetails} from "../actions/productActions";
import Loader from "../components/Loader";
import Message from "../components/Message";

const PdfViewerScreen = ({match}) => {
  const productId = match.params.id

  const dispatch = useDispatch()

  const productDetails = useSelector(state => state.productDetails)
  const {loading, error, product} = productDetails

  const [pdfFile, setPdfFile] = useState(null)

  const defaultLayoutPluginInstance = defaultLayoutPlugin()

  useEffect(() => {
    if (productId) {
      setPdfFile(product.pdf)
    }
    dispatch(listProductDetails(productId))
  }, [dispatch, match, productId, product.pdf])

  return (
    <div className='pdf-container'>
      {loading ? <Loader/> : error ? <Message variant='danger'>{error}</Message> : (
        <>

          {pdfFile &&

          <>
            <Worker workerUrl='https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js'>

              <Viewer fileUrl={pdfFile} plugins={[defaultLayoutPluginInstance]}/>
            </Worker>
          </>
          }
        </>
      )}
    </div>
  )
}

export default PdfViewerScreen;