import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import { Row, Col, Image, ListGroup, Card, Button, Form } from "react-bootstrap";

import Rating from "../components/Rating";
import {listProductDetails, createProductReview} from "../actions/productActions";
import Loader from "../components/Loader";
import Message from "../components/Message";
import {PRODUCT_CREATE_REVIEW_RESET} from "../constants/productConstants";

const ProductScreen = ({ match }) => {

const dispatch = useDispatch()

  const [rating, setRating] = useState(0)
  const [comment, setComment] = useState('')

  const productDetails = useSelector(state => state.productDetails)
  const {loading, error, product} = productDetails

  const userLogin = useSelector(state => state.userLogin)
  const {userInfo} = userLogin

  const productReviewCreate = useSelector(state => state.productReviewCreate)
  const {error:errorProductReview, success: successProductReview} = productReviewCreate

  useEffect(() => {
    if(successProductReview){
      alert('Комментарий подтвержден')
      setRating(0)
      setComment('')
      dispatch({type: PRODUCT_CREATE_REVIEW_RESET})
    }
    dispatch(listProductDetails(match.params.id))
  }, [dispatch, match, successProductReview])

  const submitHandler = e => {
  e.preventDefault()
    dispatch(createProductReview(match.params.id, {
      rating, comment
    }))
  }

  return(
    <>
      <Link className='btn btn-light my-3' to='/'>
        Назад
      </Link>
      {loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message> : (
        <>
        <Row>
          <Col md={6}>
            <Image src={product.image} alt={product.name} fluid />
          </Col>
          <Col md={3}>
            <ListGroup variant="flush">
              <ListGroup.Item className="">
                <h3>{product.name}</h3>
              </ListGroup.Item>
              <ListGroup.Item className="">
                <Rating text={`${product.numReviews} обзоров`} value={product.rating} />
              </ListGroup.Item>
              <ListGroup.Item className="">
                Описание: {product.description}
              </ListGroup.Item>
              <ListGroup.Item className="">
                Колличество страниц: {product.pageCount}
              </ListGroup.Item>
            </ListGroup>
          </Col>
          <Col md={3}>
            <Card>
              <ListGroup variant='flush'>
                <ListGroup.Item>
                  <Row>
                    <Col>
                      <Link to={`/product/${product._id}/viewer`}>
                        <Button className='btn-block' type='button'>
                          Прочитать
                        </Button>
                      </Link>
                    </Col>
                  </Row>
                </ListGroup.Item>
              </ListGroup>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <h2>Комментарии</h2>
            {product.reviews.length === 0 && <Message>Комментариев нет</Message>}
            <ListGroup variant='flush'>
              {product.reviews.map(review => (
                <ListGroup.Item key={review._id}>
                  <strong>{review.name}</strong>
                  <Rating value={review.rating} />
                  <p>{review.createdAt.substring(0, 10)}</p>
                  <p>{review.comment}</p>
                </ListGroup.Item>
              ))}
              <ListGroup.Item>
                <h2>Напишите новый комментарий</h2>
                {errorProductReview && <Message variant='danger'>{errorProductReview}</Message> }
                {userInfo ? (
                  <Form onSubmit={submitHandler}>
                    <Form.Group conrolId='rating'>
                      Рейтинг
                    <Form.Control as='select' value={rating} onChange={e => setRating(e.target.value)}>
                      <option value=''>Выбор...</option>
                      <option value='1'>1 - Очень плохо</option>
                      <option value='2'>2 - Плохо</option>
                      <option value='3'>3 - Нормально</option>
                      <option value='4'>4 - Хорошо</option>
                      <option value='5'>5 - Отлично</option>
                    </Form.Control>
                    </Form.Group>
                    <Form.Group controlId='comment'>
                      <Form.Label>Комментарий</Form.Label>
                      <Form.Control as='textarea' row='3' value={comment} onChange={e => setComment(e.target.value)}>

                      </Form.Control>
                    </Form.Group>
                    <Button type='submit' variant='primary'>
                      Подтвердить
                    </Button>
                  </Form>
                ) : <Message>Пожалуйста <Link to='/login'>авторизуйтесь</Link>, чтобы написать комментарий</Message>}
              </ListGroup.Item>
            </ListGroup>
          </Col>
        </Row>
          </>
      )}
    </>
  )
}

export default ProductScreen;