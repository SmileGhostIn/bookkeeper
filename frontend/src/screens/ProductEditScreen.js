import React, {useState, useEffect} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {Form, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
import FormContainer from "../components/FormContainer";
import {listProductDetails, updateProduct} from "../actions/productActions";
import {PRODUCT_UPDATE_RESET} from "../constants/productConstants";

const ProductEditScreen = ({match, history}) => {
  const productId = match.params.id

  const [name, setName] = useState('')
  const [pageCount, setPageCount] = useState(0)
  const [image, setImage] = useState('')
  const [pdf, setPdf] = useState('')
  const [brand, setBrand] = useState('')
  const [author, setAuthor] = useState('')
  const [countInStock, setCountInStock] = useState(0)
  const [description, setDescription] = useState('')
  const [uploading, setUploading] = useState(false)

  const dispatch = useDispatch()

  const productDetails = useSelector(state => state.productDetails)
  const {loading, error, product} = productDetails

  const productUpdate = useSelector(state => state.productUpdate)
  const {loading: loadingUpdate, error: errorUpdate, success: successUpdate} = productUpdate

  useEffect(() => {
    if (successUpdate) {
      dispatch({type: PRODUCT_UPDATE_RESET})
      history.push('/admin/productlist')
    } else {
      if (!product.name || product._id !== productId) {
        dispatch(listProductDetails(productId))
      } else {
        setName(product.name)
        setPageCount(product.pageCount)
        setImage(product.image)
        setPdf(product.pdf)
        setBrand(product.brand)
        setAuthor(product.author)
        setCountInStock(product.countInStock)
        setDescription(product.description)
      }
    }

  }, [dispatch, productId, product, successUpdate, history])

  const uploadFileHandler = async (e) => {
    const file = e.target.files[0]
    console.log(file)
    const formData = new FormData()
    formData.append('image', file)
    if (file.type === "application/pdf") {
      alert('Файл должен быть картинкой')
    }

    setUploading(true)

    try {
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }

      const {data} = await axios.post('/api/upload', formData, config)

      setImage(data)

      setUploading(false)
    } catch (e) {
      console.error(e)
      setUploading(false)

    }
  }

  const uploadPdfFileHandler = async (e) => {
    const file = e.target.files[0]
    console.log(file)
    const formData = new FormData()
    formData.append('image', file)
    if (file.type !== "application/pdf") {
      alert('Загружаемым файл должен быть .pdf')
    }
    setUploading(true)

    try {
      const config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }

      const {data} = await axios.post('/api/upload', formData, config)

      if (file.type === "application/pdf") {
        setPdf(data)
      }
      setUploading(false)
    } catch (e) {
      console.error(e)
      setUploading(false)

    }
  }

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(updateProduct({
      _id: productId,
      name,
      pageCount,
      image,
      pdf,
      brand,
      author,
      description,
      countInStock
    }))
  }

  return (
    <>
      <Link to='/admin/productList' className='btn btn-light my-3'>
        Назад
      </Link>

      <FormContainer>
        <h1>Редактирование продукта</h1>
        {loadingUpdate && <Loader/>}
        {errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
        {loading ? <Loader/> : error ? <Message variant='danger'>{error}</Message> : (
          <Form onSubmit={submitHandler}>
            <Form.Group controlId='name'>
              <Form.Label>Имя</Form.Label>
              <Form.Control type='text' placeholder='Введите имя' value={name}
                            onChange={(e) => setName(e.target.value)}>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId='pageCount'>
              <Form.Label>Цена</Form.Label>
              <Form.Control type='number' placeholder='Введите колличество страниц' value={pageCount}
                            onChange={(e) => setPageCount(e.target.value)}>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId='image'>
              <Form.Label>Картинка на превью</Form.Label>
              <Form.Control type='text' label='Введите url картинки' value={image}
                            onChange={(e) => setImage(e.target.value)}>
              </Form.Control>
              <Form.File id='image-file' label='Выберите файл' custom onChange={uploadFileHandler}>
              </Form.File>
              {uploading && <Loader/>}
            </Form.Group>

            <Form.Group controlId='pdf'>
              <Form.Label>PDF файл</Form.Label>
              <Form.Control type='text' label='Введите url к pdf файлу' value={pdf}
                            onChange={(e) => setPdf(e.target.value)}>
              </Form.Control>
              <Form.File id='pdf-file' label='Выберите файл' custom onChange={uploadPdfFileHandler}>
              </Form.File>
              {uploading && <Loader/>}
            </Form.Group>

            {/*<Form.Group controlId='brand'>*/}
            {/*  <Form.Label>Brand</Form.Label>*/}
            {/*  <Form.Control type='text' label='Введите brand' value={brand}*/}
            {/*                onChange={(e) => setBrand(e.target.value)}>*/}
            {/*  </Form.Control>*/}
            {/*</Form.Group>*/}

            {/*<Form.Group controlId='countInStock'>*/}
            {/*  <Form.Label>Count in stock</Form.Label>*/}
            {/*  <Form.Control type='text' label='Введите countInStock' value={countInStock}*/}
            {/*                onChange={(e) => setCountInStock(e.target.value)}>*/}
            {/*  </Form.Control>*/}
            {/*</Form.Group>*/}

            <Form.Group controlId='author'>
              <Form.Label>Автор</Form.Label>
              <Form.Control type='text' label='Введите автора' value={author}
                            onChange={(e) => setAuthor(e.target.value)}>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId='description'>
              <Form.Label>Описание</Form.Label>
              <Form.Control type='text' label='Введите описание' value={description}
                            onChange={(e) => setDescription(e.target.value)}>
              </Form.Control>
            </Form.Group>


            <Button type='submit' variant='primary'>
              Обновить
            </Button>
          </Form>
        )}


      </FormContainer>

    </>
  )
}

export default ProductEditScreen