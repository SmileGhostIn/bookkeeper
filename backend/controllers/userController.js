import asyncHandler from 'express-async-handler';
import generateToken from "../utils/generateToken.js";
import User from "../models/userModel.js";

/*
  @desc Аутентификация пользователя & получение токена
  @route POST /api/users/login
  @access Public
 */
const authUser = asyncHandler(async (req, res) => {
  const {email, password} = req.body

  const user = await User.findOne({ email })

  if(user && (await user.matchPassword(password))){
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateToken(user._id)
    })
  } else {
    res.status(401)
    throw new Error('Неверный e-mail или пароль')
  }
})

/*
  @desc Регистрация нового пользователя
  @route POST /api/users/login
  @access Public
 */
const registerUser = asyncHandler(async (req, res) => {
  const {name, email, password} = req.body

  const userExist = await User.findOne({ email })

 if(userExist){
   res.status(400)
   throw new Error('Пользователь уже существует')
 }

 const user = await User.create({
   name, email, password
 })

  if(user){
    res.status(201).json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateToken(user._id)
    })

  } else {
    res.status(400)
    throw new Error('Неверные данные пользователя')
  }
})


/*
  @desc Получить профиль пользователя
  @route GET /api/users/profile
  @access Private
 */
const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)

  if(user){
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateToken(user._id)
    })
  } else {
    res.status(401)
    throw new Error('Пользователь не найден')
  }
})

/*
  @desc Редактировать профиль пользователя
  @route PUT /api/users/profile
  @access Private
 */
const updateUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)

  if(user){
    user.name = req.body.name || user.name
    user.email = req.body.email || user.email
    if(req.body.password){
      user.password = req.body.password
    }

    const updatedUser = await user.save()

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: generateToken(updatedUser._id)
    })
  } else {
    res.status(404)
    throw new Error('Пользователь не найден')
  }
})


/*
  @desc Получить всех пользователей
  @route PUT /api/users
  @access Private/Admin
 */
const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({})
  res.json(users)
})

/*
  @desc Получить пользователя по id
  @route GET /api/users/:id
  @access Private/Admin
 */
const getUserById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select('-password')
  if(user){
    res.json(user)
  } else {
    res.status(404)
    throw new Error('Пользователь не найден')
  }
})

/*
  @desc Редактировать пользоваетля
  @route PUT /api/users/:id
  @access Private/Admin
 */
const updateUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id)

  if (user) {
    user.name = req.body.name || user.name
    user.email = req.body.email || user.email
    user.isAdmin = req.body.isAdmin

    const updatedUser = await user.save()

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
    })
  } else {
    res.status(404)
    throw new Error('Пользователь не найден')
  }
})

/*
  @desc Удалить пользователя
  @route DELETE /api/users/:id
  @access Private/Admin
 */
const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id)

  if(user){
    await user.remove()
    res.json({ message: 'Пользователь удален'})
  } else {
    res.status(404)
    throw new Error('Пользователь не найден')
  }
})



export {authUser, getUserProfile, registerUser, updateUserProfile, getUsers, getUserById, updateUser, deleteUser,}